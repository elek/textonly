#!/usr/bin/ruby
require 'nokogiri'
require 'open-uri'
require "net/http"
require "fileutils"
require 'liquid' 
require 'uri/http'

class Resource 
   attr_accessor :host, :resource, :content, :mime, :title
   def initialize(url, content)
      @url = url
      @content = content
      @mime = "application/xhtml+xml"
      @title = "unknown"
   end

   def path
      return @url.host + @url.path
   end 
   def id
      return @url.path.tr('/\\.','__');
   end
   def to_liquid
      {'title'=>@title,'id'=>id(),'mime'=>@mime,'url'=>@host + '/' + @resource[1..-1]}
   end

end

class TextOnly
   attr_accessor :files, :epub
   def initialize()
      @files = Array.new
      @epub = {"cover"=>false,'title'=>'testtitle','lang'=>'en','identifier'=>'textonly-2'}
      @epub['title'] = "Fogsag"
   end


def getPage(url)
   uri = URI(url)
   id = uri.host + "/" + uri.path + uri.query
   destination = "src/" + id    
   if not File.exists?(destination)
      FileUtils.mkdir_p(File.dirname(destination))
      if ENV['http_proxy']
         proxy = URI.parse(ENV['http_proxy'])
         Net::HTTP::Proxy(proxy.host,proxy.port,proxy.user,proxy.password).start(uri.host) do |http|
           File.open(destination, 'w') {|f| f.write(http.get(uri.path).body) }
         end
      else 
        http =  Net::HTTP.new(uri.host)
        request = Net::HTTP::Get.new resource
        response = http.request(uri.path)
        File.open(destination, 'w') {|f| f.write(response.body) }
       
      end
   else
      puts "File already cached " + destination
   end
   return Resource.new(uri,File.open(destination, 'r') { |f| f.read })

end

def remove(element,selector)
   element.css(selector).each do |e|
     e.remove
   end 
end

def getLocation(path)
   return "../" * (path.count ".")
end
def process(r)   
   doc = Nokogiri::HTML(r.content)
   cs = doc.css("div#main")
   remove(cs,"script")
   remove(cs,"div.mainnav")
   remove(cs,"div.botnav")
   remove(cs,"ul.tabs")
   cs.css("img").each do |e|
      img = getPage(e['src'])
      e['src'] = getLocation(r.path) + img.path
      img.mime = 'image/png'
      e['alt'] = 'unknown' if not e['alt']
      files << img
   end
   cs.css("a").each do |e|
      if e['name']
         e['id'] = e['name']
         e.delete("name")
      end
   end
   dst = "processed/" + r.path;
   FileUtils.mkdir_p(File.dirname(dst))
   File.open(dst, 'w') {|f| f.write(cs.to_html) }
   system "tidy -m -asxml " + dst   

end

def do()
   download('http://dia.jadox.pim.hu/jetspeed/displayXhtml?offset=1&origOffset=-1&docId=134&secId=12727&qdcId=3&libraryId=-1&filter=Spir%C3%B3+Gy%C3%B6rgy&limit=1000&pageSet=1&tableOfContents=true')
end

def absUrl(root, url)
   return root.scheme + "://" + root.host + url if url[0]=="/"
   return url

end


def download(urlStr)
   uri = URI(urlStr)
   i = 0
   toc = getPage(urlStr)
   doc = Nokogiri::HTML(open("src/" + toc.path))
   toc = doc.css('.tableOfContent')
   toc.css("li > a").each do |a|
         i = i + 1
         href = absUrl(uri,a["href"])
print href
        r = getPage(href)
         r.title = a.text
	 puts r.title
         @files << r
         process(r)
         break if i > 10
   end
   Dir.glob('src/**/*.png').each do |f|
      destFile = "processed/" + f[4..-1]
      FileUtils.mkdir_p(File.dirname(destFile))
      FileUtils.cp(f,destFile)
   end

   Dir.glob('src/**/*.jpg').each do |f|
      destFile = "processed/" + f[4..-1]
      FileUtils.mkdir_p(File.dirname(destFile))
      FileUtils.cp(f,destFile)
   end


   write_epub("META-INF/container.xml")
   write_epub("mimetype")
   write_epub("content.opf")
   write_epub("toc.ncx")
  
end


def write_epub(template_file)
   dest = File.join("processed", template_file)
   FileUtils.mkdir_p(File.dirname(dest))
   template_content = Liquid::Template.parse(File.open("../templates/" + template_file).read).render( 'epub' => @epub, 'files' => @files )
   File.open(dest, "w" ) { |f| f.write(template_content) }
end

end

t = TextOnly.new()
t.do
